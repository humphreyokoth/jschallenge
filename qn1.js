function findOdd(A) {
    //happy coding!

    // Declared variable count and initialised.
let count = 0;
// Created a for loop to iterate through each number in the array A.
// Looped through each Item in Array of A and  counted the number of times the number occured in the arrayusing nested loop.
for(let i=0;i<A.length;i++){
    
    for(let oddNum = 0;oddNum<A.length;oddNum++){
        //checking if item in A is same as Item in oddNum then adding to count. 
        if(A[i] == A[oddNum]){
            count++;
        }
    }
//  Divided each number we counted if it is not equal to 0 
    if(count % 2 !== 0){

        return A[i];

    }
}
return 0;


}

// Tested the function .
console.log(findOdd([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5]));
console.log(findOdd([1,1,2,-2,5,2,4,4,-1,-2,5]));
console.log(findOdd([1,1,1,1,1,1,10,1,1,1,1]));
 


